$(document).ready(function () {

	$( window ).scroll(function() {
		if($(document).scrollTop() > 100) {
			$('.page__header').addClass('page__header--small');
		} else {
			$('.page__header').removeClass('page__header--small');
		}
	});
	
	$('.nav-toggle').click(function(){
		$('body').toggleClass('nav-open');
	});
	
	$('.nav-toggle-close').click(function(){
		$('body').removeClass('nav-open');
	});
	
	$('.carousel--jumbotron').carousel({
		interval: 4000
	});
	
	$('.carousel--impression').carousel({
		interval: 4000
	});

	$('.js-toggle-map').click(function(){
		$('.footer__map').toggleClass('open');
	});
    
    $('.page__aside a[href^="#"]').click(function(event) {
        // The id of the section we want to go to.
        var id = '#brochure';

        // An offset to push the content down from the top.
        var offset = $(window).height()/2 - $(id).height() / 2;

        // Our scroll target : the top position of the
        // section that has the id referenced by our href.
        var target = $(id).offset().top - 0;

        // The magic...smooth scrollin' goodness.
        $('html, body').animate({scrollTop:target},{duration:2000, easing:'easeInOutQuint'});

        //prevent the page from jumping down to our section.
        event.preventDefault();
    });
    
    var map = $('#map_canvas');
    map.gMap({
        address: map.data('address'),
        zoom: 15,
        markers:[{
            address:map.data('address'),
            html: map.data('info'),
            popup: true
        }]
    });
});



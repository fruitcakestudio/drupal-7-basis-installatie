<?php include(drupal_get_path('theme', 'twitter_bootstrap').'/templates/partials/page-header.tpl.php'); ?>

<section class="jumbotron">
	<div id="main-slider" class="jumbotron-slider carousel--jumbotron carousel slide carousel-fade">
		<?php 
            
		$query = new EntityFieldQuery();
		$query->entityCondition('entity_type', 'node')
		  ->propertyCondition('status', 1)
		  ->propertyCondition('type', array('slide'))
		  ->propertyCondition('language', $language->language)
		  ->propertyOrderBy('title', 'ASC')
		  ->range(0, 10);
		
		$result = $query->execute();
		
		$nodes = node_load_multiple(array_keys($result['node']));	
		
		?>
            
        <div class="carousel-inner"> 
	
			<?php 
			$first_slide = true;
			$j = 0;
			foreach ($nodes as $slide) { 
			
				$j++;
				
				if ($first_slide) {
					$active_slide = "active";
				} else {
					$active_slide = "";
				}
				
				$first_slide = false;				

				$background_img = $slide->field_slide_afbeelding['und'][0]['uri'];
				
				?>	
				<div class="item <?php echo $active_slide; ?>" style="background-image: url('<?php print image_style_url("slide",$background_img); ?>');">
					<div class="darken"></div>
					<div class="container">
						<div class="row">
							<div class="slider__caption">
								<?php print render($slide->body['und'][0]['value']); ?>
								<a class="slider__link" href="<?php print render($slide->field_link['und'][0]['url']); ?>"><?php print t('Read more'); ?></a>
							</div>
						</div>
					</div>
				</div>

				
				
			
			<?php } ?>	            
		
			
		</div>
		
		<!-- Controls -->
		  <a class="left carousel-control" href="#main-slider" role="button" data-slide="prev">
		    <span class="fa fa-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#main-slider" role="button" data-slide="next">
		    <span class="fa fa-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
    </div>
</section>

<section class="section section--main" role="main">
	<div class="container">
		<div class="row">
			<div class="page-content">
				<?php print render($page['content']); ?>
			</div>
			<aside class="page-aside">
				<?php print render($page['content-aside']); ?>
			</aside>
		</div>
	</div>
</section>

<?php include(drupal_get_path('theme', 'twitter_bootstrap').'/templates/partials/page-footer.tpl.php'); ?>
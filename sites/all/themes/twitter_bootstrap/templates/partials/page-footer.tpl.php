<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="page-block">
				<?php print render($page['footer-contact']); ?>
			</div>
			<div class="page-block page-block--wide">
				<?php print render($page['footer-socialmedia']); ?>
			</div>
		</div>
	</div>
</footer>
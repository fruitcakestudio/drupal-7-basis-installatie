<header class="page-header">
	<div class="container">
		<div class="row">
			<div class="brand">
				<?php if ($logo): ?>
					<?php $site_name = variable_get('site_name'); ?>
		            <a class="brand-link" href="<?php print $front_page; ?>" title="<?= $site_name ?>">
		                <img class="brand-logo" src="<?php print $logo; ?>" alt="<?= $site_name ?>" />
		            </a>
		        <?php endif; ?>
			</div>
			
			<nav class="primaryNavigation">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<?php if ($page['navigation']): ?>
				        <?php print render($page['navigation']); ?>
				    <?php endif; ?>
				</div>
			</nav>
		</div>
	</div>
</header>
<?php include(drupal_get_path('theme', 'twitter_bootstrap').'/templates/partials/page-header.tpl.php'); ?>

<section class="section section--main" role="main">
	<div class="container">
		<?php print render($title_prefix); ?>
		<?php print render($title_suffix); ?>
		<?php if ($tabs): ?>
		<div class="tabs">
		<?php print render($tabs); ?>
		</div>
		<?php endif; ?>
		<?php print $messages; ?>
		<?php if ($action_links): ?>
		<ul class="action-links"><?php print render($action_links); ?></ul>
		<?php endif; ?>

		<?php print render($page['content']); ?>	
	</div>
</section>

<?php include(drupal_get_path('theme', 'twitter_bootstrap').'/templates/partials/page-footer.tpl.php'); ?>